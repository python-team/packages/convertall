<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="es_ES">
<context>
    <name>cmdline</name>
    <message>
        <location filename="cmdline.py" line="22"/>
        <source>Usage:</source>
        <translation>Uso:</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="24"/>
        <source>qt-options</source>
        <translation>opciones-qt</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="32"/>
        <source>-or- (non-GUI):</source>
        <translation>-o- (sin interfaz gráfica):</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="33"/>
        <source>options</source>
        <translation>opciones</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="28"/>
        <source>number</source>
        <translation>número</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="29"/>
        <source>from_unit</source>
        <translation>desde_unidad</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="30"/>
        <source>to_unit</source>
        <translation>a_unidad</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="35"/>
        <source>Units with spaces must be &quot;quoted&quot;</source>
        <translation>Unidades con espacios deben encerrarse entre &quot;comillas&quot;</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="37"/>
        <source>Options:</source>
        <translation>Opciones:</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="38"/>
        <source>num</source>
        <translation>núm</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="39"/>
        <source>set number of decimals to show</source>
        <translation>seleccione el número de decimales a mostrar</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="41"/>
        <source>show set number of decimals, even if zeros</source>
        <translation>mostrar el número seleccionado de decimales, incluso si son ceros</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="47"/>
        <source>display this message and exit</source>
        <translation>mostrar este mensaje y salir</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="49"/>
        <source>interactive command line mode (non-GUI)</source>
        <translation>modo interactivo en línea de comandos (no interfaz)</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="51"/>
        <source>convert without further prompts</source>
        <translation>convertir sin más interacción</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="43"/>
        <source>show results in scientific notation</source>
        <translation>mostrar resultados en notación científica</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="116"/>
        <source>Enter from unit -&gt; </source>
        <translation>Ingresar unidad de origen -&gt;</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="122"/>
        <source>Enter to unit -&gt; </source>
        <translation>Ingresar unidad de destino -&gt;</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="139"/>
        <source>Enter number, [n]ew, [r]everse or [q]uit -&gt; </source>
        <translation>Ingresar número, [n]uevo, [i]nverso o [s]alir</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="157"/>
        <source>Units {0} and {1} are not compatible</source>
        <translation>Las unidades no son compatibles ({0}  vs.  {1})</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="173"/>
        <source>{0} is not a valid unit</source>
        <translation>{0} no es una unidad válida</translation>
    </message>
    <message>
        <location filename="cmdline.py" line="45"/>
        <source>show results in engineering notation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>convertdlg</name>
    <message>
        <location filename="convertdlg.py" line="64"/>
        <source>%d units loaded</source>
        <translation type="obsolete">%d unidades leídas</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="67"/>
        <source>Error in unit data - %s</source>
        <translation type="obsolete">Error en los datos de las unidades - %s</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="85"/>
        <source>From Unit</source>
        <translation>Desde unidad</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="100"/>
        <source>To Unit</source>
        <translation>Hacia unidad</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="117"/>
        <source>Set units</source>
        <translation>Seleccionar unidades</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="134"/>
        <source>No Unit Set</source>
        <translation>Ninguna unidad seleccionada</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="152"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="156"/>
        <source>&amp;Unit Finder...</source>
        <translation>B&amp;uscador de unidades...</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="160"/>
        <source>&amp;Options...</source>
        <translation>&amp;Opciones...</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="164"/>
        <source>&amp;Help...</source>
        <translation>A&amp;yuda...</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="168"/>
        <source>&amp;About...</source>
        <translation>&amp;Acerca de...</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="207"/>
        <source>Clear Unit</source>
        <translation>Limpiar unidad</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="283"/>
        <source>Result Display</source>
        <translation>Pantalla de resultados</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="280"/>
        <source>Decimal places</source>
        <translation>Lugares decimales</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="286"/>
        <source>Use scientific notation</source>
        <translation>Utilizar notación científica</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="285"/>
        <source>Use fixed decimal places</source>
        <translation>Utilizar un número fijo de decimales</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="277"/>
        <source>Buttons</source>
        <translation type="obsolete">Botones</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="294"/>
        <source>Show operator buttons</source>
        <translation>Mostrar los botones con los operadores</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="297"/>
        <source>Colors</source>
        <translation>Colores</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="299"/>
        <source>Use default system colors</source>
        <translation>Usar colores definidos por el sistema</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="300"/>
        <source>Set background color</source>
        <translation>Seleccionar el color de fondo</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="301"/>
        <source>Set text color</source>
        <translation>Seleccionar el color del texto</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="379"/>
        <source>Read Me file not found</source>
        <translation>Archivo &quot;léame&quot; no encontrado</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="382"/>
        <source>ConvertAll README File</source>
        <translation>Archivo LÉAME de ConvertAll</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="365"/>
        <source>ConvertAll Version %s
by %s</source>
        <translation type="obsolete">Versión de ConvertAll %s
por %s</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="216"/>
        <source>Recent Unit</source>
        <translation>Unidad reciente</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="288"/>
        <source>Recent Units</source>
        <translation>Unidades recientes</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="289"/>
        <source>Number saved</source>
        <translation>Número salvado</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="68"/>
        <source>Error in unit data - {0}</source>
        <translation>Error en los datos de las unidades - {0}</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="71"/>
        <source>{0} units loaded</source>
        <translation>{0} unidades leídas</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="291"/>
        <source>Load last units at startup</source>
        <translation>Cargue últimas unidades en el arranque</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="292"/>
        <source>User Interface</source>
        <translation>Interfaz de usuario</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="296"/>
        <source>Show tip at startup</source>
        <translation>Mostrar toque en el arranque</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="390"/>
        <source>ConvertAll Version {0}
by {1}</source>
        <translation>Versión de ConvertAll {0}
por {1}</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="418"/>
        <source>Convertall - Tip</source>
        <translation>ConvertAll</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="444"/>
        <source>&amp;OK</source>
        <translation>&amp;Aceptar</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="422"/>
        <source>Combining Units</source>
        <translation>La combinación de Unidades</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="431"/>
        <source>&lt;p&gt;ConvertAll&apos;s strength is the ability to combine units:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Enter &quot;m/s&quot; to get meters per second&lt;/li&gt;&lt;li&gt;Enter &quot;ft*lbf&quot; to get foot-pounds (torque)&lt;/li&gt;&lt;li&gt;Enter &quot;in^2&quot; to get square inches&lt;/li&gt;&lt;li&gt;Enter &quot;m^3&quot; to get cubic meters&lt;/li&gt;&lt;li&gt;or any other combinations you can imagine&lt;/li&gt;</source>
        <translation type="obsolete">&lt;p&gt;La fuerza de ConvertAll es la capacidad de combinar unidades:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;introduzca &quot;m / s&quot; para obtener metros por segundo&lt;/li&lt;li&gt;Enter &quot;ft * lbf&quot; para obtener libras-pie (par)&lt;/li&gt;&lt;li&gt;Enter &quot;en ^ 2&quot; para pulgadas cuadradas&lt;/li&gt;&lt;li&gt;Enter &quot;m ^ 3 &quot;para obtener metros cúbicos&lt;/li&lt;li&gt;o cualquier otra combinación que se pueda imaginar&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="439"/>
        <source>Show this tip at startup</source>
        <translation>Mostrar toque en el arranque</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="434"/>
        <source>&lt;p&gt;ConvertAll&apos;s strength is the ability to combine units:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Enter &quot;m/s&quot; to get meters per second&lt;/li&gt;&lt;li&gt;Enter &quot;ft*lbf&quot; to get foot-pounds (torque)&lt;/li&gt;&lt;li&gt;Enter &quot;in^2&quot; to get square inches&lt;/li&gt;&lt;li&gt;Enter &quot;m^3&quot; to get cubic meters&lt;/li&gt;&lt;li&gt;or any other combinations you can imagine&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;La fuerza de ConvertAll es la capacidad de combinar unidades:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;introduzca &quot;m / s&quot; para obtener metros por segundo&lt;/li&lt;li&gt;Enter &quot;ft * lbf&quot; para obtener libras-pie (par)&lt;/li&gt;&lt;li&gt;Enter &quot;in ^ 2&quot; para pulgadas cuadradas&lt;/li&gt;&lt;li&gt;Enter &quot;m ^ 3 &quot;para obtener metros cúbicos&lt;/li&lt;li&gt;o cualquier otra combinación que se pueda imaginar&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="279"/>
        <source>Result Precision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="284"/>
        <source>Use short representation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="convertdlg.py" line="287"/>
        <source>Use engineering notation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>finddlg</name>
    <message>
        <location filename="finddlg.py" line="28"/>
        <source>Unit Finder</source>
        <translation>Buscador de unidades</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="35"/>
        <source>&amp;Filter Unit Types</source>
        <translation>&amp;Filtro de tipos de unidades</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="41"/>
        <source>&amp;Search String</source>
        <translation>Bu&amp;scar cadena</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="46"/>
        <source>C&amp;lear</source>
        <translation>&amp;Limpiar</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="55"/>
        <source>From Unit</source>
        <translation>Desde unidad</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="58"/>
        <source>&amp;Replace</source>
        <translation>&amp;Reemplazar</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="61"/>
        <source>&amp;Insert</source>
        <translation>&amp;Insertar</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="64"/>
        <source>To Unit</source>
        <translation>A unidad</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="67"/>
        <source>Replac&amp;e</source>
        <translation>Re&amp;emplazar</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="70"/>
        <source>Inser&amp;t</source>
        <translation>Inser&amp;tar</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="76"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="194"/>
        <source>Unit Name</source>
        <translation>Nombre de la unidad</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="194"/>
        <source>Unit Type</source>
        <translation>Tipo de unidad</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="194"/>
        <source>Comments</source>
        <translation>Comentarios</translation>
    </message>
    <message>
        <location filename="finddlg.py" line="135"/>
        <source>[All]</source>
        <translation>[Todo]</translation>
    </message>
</context>
<context>
    <name>helpview</name>
    <message>
        <location filename="helpview.py" line="43"/>
        <source>&amp;Back</source>
        <translation>&amp;Retroceder</translation>
    </message>
    <message>
        <location filename="helpview.py" line="51"/>
        <source>&amp;Forward</source>
        <translation>&amp;Avanzar</translation>
    </message>
    <message>
        <location filename="helpview.py" line="59"/>
        <source>&amp;Home</source>
        <translation>&amp;Inicio</translation>
    </message>
    <message>
        <location filename="helpview.py" line="67"/>
        <source>Find</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="helpview.py" line="74"/>
        <source>Find &amp;Previous</source>
        <translation>&amp;Buscar anterior</translation>
    </message>
    <message>
        <location filename="helpview.py" line="81"/>
        <source>Find &amp;Next</source>
        <translation>Buscar &amp;siguiente</translation>
    </message>
    <message>
        <location filename="helpview.py" line="114"/>
        <source>Text string not found</source>
        <translation>Cadena de texto que no se encuentra</translation>
    </message>
</context>
<context>
    <name>numedit</name>
    <message>
        <location filename="numedit.py" line="92"/>
        <source>Error in unit data - %s</source>
        <translation type="obsolete">Error en los datos de las unidades - %s</translation>
    </message>
    <message>
        <location filename="numedit.py" line="57"/>
        <source>Converting...</source>
        <translation>Convirtiendo...</translation>
    </message>
    <message>
        <location filename="numedit.py" line="66"/>
        <source>Units are not compatible (%s  vs.  %s)</source>
        <translation type="obsolete">Las unidades no son compatibles (%s  vs.  %s)</translation>
    </message>
    <message>
        <location filename="numedit.py" line="76"/>
        <source>Set units</source>
        <translation>Seleccionar unidades</translation>
    </message>
    <message>
        <location filename="numedit.py" line="77"/>
        <source>No Unit Set</source>
        <translation>Ninguna unidad seleccionada</translation>
    </message>
    <message>
        <location filename="numedit.py" line="102"/>
        <source>Error in unit data - {0}</source>
        <translation>Error en los datos de las unidades - {0}</translation>
    </message>
    <message>
        <location filename="numedit.py" line="70"/>
        <source>Units are not compatible ({0}  vs.  {1})</source>
        <translation>Las unidades no son compatibles ({0}  vs.  {1})</translation>
    </message>
</context>
<context>
    <name>optiondlg</name>
    <message>
        <location filename="optiondlg.py" line="38"/>
        <source>&amp;OK</source>
        <translation>&amp;Aceptar</translation>
    </message>
    <message>
        <location filename="optiondlg.py" line="41"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="optiondlg.py" line="44"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
</context>
<context>
    <name>unitatom</name>
    <message>
        <location filename="unitatom.py" line="45"/>
        <source>Bad equation for &quot;%s&quot;</source>
        <translation type="obsolete">Ecuación equivocada para &quot;%s&quot;</translation>
    </message>
    <message>
        <location filename="unitatom.py" line="45"/>
        <source>Bad equation for &quot;{0}&quot;</source>
        <translation>Ecuación equivocada para &quot;{0}&quot;</translation>
    </message>
</context>
<context>
    <name>unitdata</name>
    <message>
        <location filename="unitdata.py" line="60"/>
        <source>Can not read &quot;units.dat&quot; file</source>
        <translation>No se puede leer el archivo &quot;units.dat&quot;</translation>
    </message>
    <message>
        <location filename="unitdata.py" line="86"/>
        <source>Duplicate unit names found</source>
        <translation>Se ha encontrado un nombre de unidad duplicado</translation>
    </message>
</context>
<context>
    <name>unitgroup</name>
    <message>
        <location filename="unitgroup.py" line="302"/>
        <source>Circular unit definition</source>
        <translation>Definición de unidad circular</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="286"/>
        <source>Invalid conversion for &quot;%s&quot;</source>
        <translation type="obsolete">Conversión no válida para &quot;%s&quot;</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="356"/>
        <source>Cannot combine non-linear units</source>
        <translation>No pueden combinarse unidades no lineares</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="368"/>
        <source>Bad equation for %s</source>
        <translation type="obsolete">Ecuación equivocada para &quot;%s&quot;</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="307"/>
        <source>Invalid conversion for &quot;{0}&quot;</source>
        <translation>Conversión no válida para &quot;{0}&quot;</translation>
    </message>
    <message>
        <location filename="unitgroup.py" line="395"/>
        <source>Bad equation for {0}</source>
        <translation>Ecuación equivocada para &quot;{0}&quot;</translation>
    </message>
</context>
<context>
    <name>unitlistview</name>
    <message>
        <location filename="unitlistview.py" line="30"/>
        <source>Unit Name</source>
        <translation>Nombre de la unidad</translation>
    </message>
    <message>
        <location filename="unitlistview.py" line="30"/>
        <source>Unit Type</source>
        <translation>Tipo de unidad</translation>
    </message>
    <message>
        <location filename="unitlistview.py" line="30"/>
        <source>Comments</source>
        <translation>Comentarios</translation>
    </message>
</context>
</TS>
